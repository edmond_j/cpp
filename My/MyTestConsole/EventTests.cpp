#include <string>
#include <iostream>
#include "Event.hpp"

using namespace My;
using namespace std;

namespace EventTests
{
	class Observable
	{
	public:
		typedef Event<const Observable&, const string&> OnStringEvent;

		const OnStringEvent::HandlerList& getOnStringEvent() const
		{
			return this->_onStringEvent;
		}

		void triggerString(const string& str) const
		{
			cout << "Event triggered: " << str << endl;
			this->_onStringEvent(*this, str);
		}

	private:
		OnStringEvent _onStringEvent;
	};

	class Observer
	{
	public:
		void onStringEvent(const Observable&, const string& str)
		{
			cout << "Event received: " << str << endl;
		}
	};

	void Run()
	{
		Observable a;
		Observer e;

		a.triggerString("L1");
		a.getOnStringEvent().add(Observable::OnStringEvent::Handler(e, &Observer::onStringEvent));
		a.triggerString("L2");
		a.getOnStringEvent().add(Observable::OnStringEvent::Handler(e, &Observer::onStringEvent));
		a.triggerString("L3");
		a.getOnStringEvent().remove(Observable::OnStringEvent::Handler(e, &Observer::onStringEvent));
		a.triggerString("L4");
		a.getOnStringEvent().remove(Observable::OnStringEvent::Handler(e, &Observer::onStringEvent));
		a.triggerString("L5");
	}
}