#include <list>
#include "Using.hpp"

using namespace std;
using namespace My;

namespace UsingTests
{
	void Run()
	{
		list<Using<int>> l;

		l.push_back(Using<int>(new int(4)));
	}
}