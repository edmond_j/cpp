#include <iostream>
#include "FileSystem.hpp"

using namespace My;
using namespace std;

namespace FileSystemTests
{
	void Run()
	{
		list<string> files(getFilesInDirectory("."));

		for (list<string>::iterator it(files.begin()); it != files.end(); ++it)
		{
			cout << *it << endl;
		}
	}
}