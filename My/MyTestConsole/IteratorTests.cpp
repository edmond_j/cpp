#include <iostream>
#include <list>
#include <vector>
#include "Enumerator.hpp"
#include "Using.hpp"

using namespace std;
using namespace My;

namespace IteratorTests
{
	class EvenSelector
	{
	public:
		bool isEven(int& i)
		{
			return !!(i % 2);
		}
	};

	bool isodd(int& i)
	{
		return !(i % 2);
	}

	void Run()
	{

		vector<int> v = { 1, 2, 3, 4, 5, 6 };
		vector<int*> pv;

		for (Enumerator<int> e(v); e; ++e)
		{
			if (*e == 3)
				*e = 5;
			cout << "Addr: " << &(*e) << endl;
			cout << *e << endl;
			pv.push_back(&(*e));
		}
		
		cout << "Pointer version" << endl;
		
		v[2] = 3;

		for (Enumerator<int*> e(pv); e; ++e)
		{
			cout << **e << endl;
		}
		
		for (Enumerator<int> e(pv); e; ++e)
		{
			cout << "Addr" << e.operator->() << endl;
			cout << *e << endl;
		}
		
		cout << "End test" << endl;

		vector<string*> critters = { new string("Hello"), new string("World"), new string("SOAIDJWO") };

		Enumerator<const string> e1(critters);

		for (Enumerator<const string> e2((const Enumerator<const string>&)e1); e2; ++e2)
		{
			cout << e2->length() << endl;
			delete &(*e2);
		}

		cout << "Test filter: odd" << endl;
		for (Enumerator<int> e(Enumerator<int>(v).filter(&isodd)); e; ++e)
		{
			cout << *e << endl;
		}

		cout << "Test filter: even" << endl;

		EvenSelector es;
		for (Enumerator<int> e(Enumerator<int>(v).filter(Action<bool, int&>(es, &EvenSelector::isEven))); e; ++e)
		{
			cout << *e << endl;
		}

	}
}
