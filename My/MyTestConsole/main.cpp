#include <iostream>

using namespace std;

namespace IteratorTests { void Run(); }

namespace UsingTests { void Run(); }

namespace ActionTests { void Run(); }

namespace EventTests { void Run(); }

namespace FileSystemTests { void Run(); }

int main()
{
//	ActionTests::Run();
//	UsingTests::Run();
//	IteratorTests::Run();
	EventTests::Run();
	//FileSystemTests::Run();
	cin.get();
}