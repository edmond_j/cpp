#include <string>
#include <iostream>
#include "Action.hpp"

using namespace std;
using namespace My;

namespace ActionTests
{
	struct A
	{
		int i;

		void print(const string& str)
		{
			cout << str << ' ' << i << endl;
		}
	};

	int add(int x, int y)
	{
		return x + y;
	}

	void Run()
	{
		A a1 = { 1337 };
		A a2 = { 666 };

		Action<int, int, int> act(&add);
		cout << act(3, 5) << endl;
		Action<int, int, int> act2(&add);
		cout << (act == act2) << endl;

		Action<void, A*, const string&> memact1(&A::print);
		Action<void, A*, const string&> memact2(&A::print);
		memact1(&a1, "Coucou");
		memact1(&a2, "Coucou");
		cout << (memact1 == memact2) << endl;

		Action<void, const string&> bf1(a1, &A::print);
		Action<void, const string&> bf2(a1, &A::print);
		Action<void, const string&> bf3(a2, &A::print);
		bf1("KOUK");
		bf2("JOUGE");
		bf3("FLIP FLOP");
		cout << (bf1 == bf2) << endl;
		cout << (bf2 == bf3) << endl;

		{
			Action<int, int, int> copy(act);

			cout << copy(78, -60) << endl;
			cout << (act == copy) << endl;
		}
		cout << act(3, 5) << endl;

		Action<void, A&, const string&> refmemact1(&A::print);
		Action<void, A&, const string&> refmemact2(&A::print);
		refmemact1(a1, "Coucou");
		refmemact1(a2, "Coucou");
		cout << (refmemact1 == refmemact2) << endl;
	}
}