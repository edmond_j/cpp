/*
Copyright Julien Edmond (05/06/2014)

edmondju@gmail.com

This library is a C++ library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#pragma once

#include <list>
#include <algorithm>
#include "TypeTraits.h"
#include "Action.hpp"
#include "CollectionUtil.hpp"

namespace My
{
	template<typename TSender, typename Handler, bool>
	class _EventHandlerList
	{
	public:
		_EventHandlerList() = default;
		_EventHandlerList(const _EventHandlerList& other) = default;
		_EventHandlerList& operator=(const _EventHandlerList& other) = default;
		virtual ~_EventHandlerList() = default;

		void add(const Handler& handler)
		{
			this->_handlers.push_back(handler);
		}

		void remove(const Handler& handler)
		{
			RemoveFirst(this->_handlers, handler);
		}
	protected:
		std::list<Handler> _handlers;
	};

	template<typename TSender, typename Handler>
	class _EventHandlerList<TSender, Handler, true> : public _EventHandlerList<TSender, Handler, false>
	{
	public:
		_EventHandlerList() = default;
		_EventHandlerList(const _EventHandlerList& other) = default;
		_EventHandlerList& operator=(const _EventHandlerList& other) = default;
		virtual ~_EventHandlerList() = default;

		void add(const Handler& handler) const
		{
			const_cast<_EventHandlerList*>(this)->_handlers.push_back(handler);
		}

		void remove(const Handler& handler) const
		{
			RemoveFirst(const_cast<_EventHandlerList*>(this)->_handlers, handler);
		}
	};

	template<typename TSender, typename... TArgs>
	class EventHandlerList : public _EventHandlerList<TSender, Action<void, TSender, TArgs...>, std::is_const<typename RemoveAllPR<TSender>::value>::value>
	{
	public:
		typedef _EventHandlerList<TSender, Action<void, TSender, TArgs...>, std::is_const<typename RemoveAllPR<TSender>::value>::value> Base;
		typedef Action<void, TSender, TArgs...> Handler;

		EventHandlerList() = default;
		EventHandlerList(const EventHandlerList& other) : Base(other) {}
		EventHandlerList& operator=(const EventHandlerList& other) = default;
		virtual ~EventHandlerList() throw() {};
	};
}