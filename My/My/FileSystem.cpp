/*
Copyright Julien Edmond (05/06/2014)

edmondju@gmail.com

This library is a C++ library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include "FileSystem.hpp"

#ifdef _WIN32

# include <Windows.h>

#else

# include <dirent.h>

#endif

using namespace std;

namespace My
{
	list<string> getFilesInDirectory(const string& directoryName)
	{
		list<string> sent;

		getFilesInDirectory(directoryName, sent);
		return sent;
	}

	void getFilesInDirectory(const string& directoryName, list<string>& output)
	{
#ifdef _WIN32
		WIN32_FIND_DATA ffd;
		HANDLE hFind;

		hFind = FindFirstFile((directoryName + "\\*").c_str(), &ffd);

		if (hFind != INVALID_HANDLE_VALUE)
		{
			do
			{
				output.push_back(ffd.cFileName);
			}
			while (FindNextFile(hFind, &ffd));
			FindClose(hFind);
		}
#else
		DIR *dir;
		struct dirent *ent;

		if ((dir = opendir(directoryName.c_str())) != nullptr) 
		{
			while ((ent = readdir(dir)) != nullptr) 
			{
				output.push_back(ent->d_name);
			}
			closedir(dir);
		}
#endif
	}
}
