/*
Copyright Julien Edmond (05/06/2014)

edmondju@gmail.com

This library is a C++ library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#pragma once

#include "IAction.hpp"

namespace My
{
	template<typename TRet, typename... TArgs>
	class FunctionAction : public IAction <TRet, TArgs...>
	{
	public:
		typedef TRet(*func_type)(TArgs...);

		FunctionAction(func_type func)
			: _func(func)
		{}

		FunctionAction(const FunctionAction& other)
			: _func(other._func)
		{}

		FunctionAction& operator=(const FunctionAction& other)
		{
			if (this != &other)
			{
				this->_func = other._func;
			}
			return *this;
		}

		~FunctionAction()
		{}

		TRet exec(TArgs... args)
		{
			return this->_func(args...);
		}

		bool operator==(const FunctionAction& other) const
		{
			return this->_func == other._func;
		}

		bool operator==(const IAction<TRet, TArgs...>& other) const
		{
			const FunctionAction* ptr(dynamic_cast<const FunctionAction*>(&other));

			return ptr != nullptr && *this == *ptr;
		}

	private:
		func_type _func;
	};
}