/*
Copyright Julien Edmond (05/06/2014)

edmondju@gmail.com

This library is a C++ library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#pragma once

#include "IEnumerator.hpp"
#include "Action.hpp"

namespace My
{
	template<typename T>
	class FilterEnumerator : public IEnumerator<T>
	{
	private:
		SmartPointer<IEnumerator<T>> _wrapped;
		Action<bool, T&> _predicate;
		T* _value;

	public:
		FilterEnumerator(IEnumerator<T>* enumerator, Action<bool, T&> predicate)
			: _wrapped(enumerator), _predicate(predicate)
		{
			this->moveNext();
		}

		FilterEnumerator(const FilterEnumerator& other)
			: _wrapped(other._wrapped), _predicate(other._predicate)
		{
			this->moveNext();
		}

		FilterEnumerator&	operator=(const FilterEnumerator& other)
		{
			if (this != &other)
			{
				this->_wrapped = other._wrapped.value();
				this->_predicate = other._predicate;
				this->_value = other._value;
			}
			return *this;
		}

		virtual ~FilterEnumerator()
		{
			if (this->_wrapped.getReferenceCount() <= 1)
				delete this->_wrapped;
		}

		bool hasValue() const
		{
			return this->_value != nullptr;
		}

		T& value()
		{
			return *this->_value;
		}

		void moveNext()
		{
			this->_value = nullptr;

			while (this->_wrapped->hasValue() && this->_value == nullptr)
			{
				if (this->_predicate(this->_wrapped->value()))
					this->_value = &this->_wrapped->value();
				this->_wrapped->moveNext();
			}
		}
	};
}