/*
Copyright Julien Edmond (05/06/2014)

edmondju@gmail.com

This library is a C++ library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#pragma once

#include "SmartPointer.hpp"
#include "IAction.hpp"
#include "FunctionAction.hpp"
#include "MemFunAction.hpp"
#include "BindMethodAction.hpp"
#include "RefMemFunAction.hpp"

namespace My
{
	template<typename TRet, typename... TArgs>
	class Action
	{
	private:
		SmartPointer<IAction<TRet, TArgs...> > _action;

		Action(IAction<TRet, TArgs...>* action)
			: _action(action)
		{}

	public:
		template<typename TObj, typename... TArgs2>
		Action(TObj& obj, TRet (TObj::*func)(TArgs2...))
			: _action(new BindMethodAction<TRet, TObj, TArgs2...>(obj, func))
		{}

		template<typename TObj, typename... TArgs2>
		Action(TObj* obj, TRet (TObj::*func)(TArgs2...))
			: _action(new BindMethodAction<TRet, TObj, TArgs2...>(obj, func))
		{}

		template<typename TObj, typename... TArgs2>
		Action(TRet (TObj::*func)(TArgs2...))
			: _action(new MemFunAction<TRet, TObj, TArgs2...>(func))
		{}

		Action(TRet(*func)(TArgs...))
			: _action(new FunctionAction<TRet, TArgs...>(func))
		{}

		Action(const Action& other)
			: _action(other._action)
		{}

		Action& operator=(const Action& other)
		{
			if (this != &other)
			{
				if (this->_action.getReferenceCount() <= 1)
					delete this->_action;
				this->_action = other._action;
			}
			return *this;
		}

		Action(Action&& other)
			: _action(other._action)
		{
			other._action = nullptr;
		}

		~Action()
		{
			if (this->_action.getReferenceCount() <= 1)
				delete this->_action;
		}

		TRet operator()(TArgs... args)
		{
			return this->_action->exec(args...);
		}

		bool operator==(const Action& other)
		{
			return this == &other || *this->_action == *other._action;
		}
	};
}