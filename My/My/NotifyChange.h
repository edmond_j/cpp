#include "Event.hpp"

template<typename TThis>
class NotifyChange
{
private:
	My::Event<TThis> _notifyChange;

public:
	NotifyChange() = default;
	NotifyChange(const NotifyChange& other) = default;
	NotifyChange& operator=(const NotifyChange& other) = default;
	~NotifyChange() = default;

	const My::EventHandlerList& getOnNotifyChange() const
	{
		return this->_notifyChange;
	}

protected:
	void triggerNotifyChange()
	{
		this->_notifyChange(*this);
	}
};