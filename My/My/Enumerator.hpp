/*
Copyright Julien Edmond (05/06/2014)

edmondju@gmail.com

This library is a C++ library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#pragma once

#include "IEnumerator.hpp"
#include "Using.hpp"
#include "SmartPointer.hpp"
#include "IteratorEnumerator.hpp"
#include "IndirectionEnumerator.hpp"
#include "FilterEnumerator.hpp"

namespace My
{
	template<typename T>
	class Enumerator
	{
	private:
		SmartPointer<IEnumerator<T> > _enumerator;

		template<typename TIterator, typename TValue>
		struct indirectionSelecter
		{
			static IEnumerator<T>* create(const TIterator& from, const TIterator& to)
			{
				return new IteratorEnumerator<T, TIterator>(from, to);
			}
		};

		template<typename TIterator>
		struct indirectionSelecter<TIterator, typename std::remove_const<T>::type *>
		{
			static IEnumerator<T>* create(const TIterator& from, const TIterator& to)
			{
				return new IndirectionEnumerator<T>(new IteratorEnumerator<T*, TIterator>(from, to));
			}
		};

		template<typename TIterator>
		static IEnumerator<T>* createEnumerator(const TIterator& from, const TIterator& to)
		{
			return indirectionSelecter<TIterator, typename std::remove_const<typename TIterator::value_type>::type >::create(from, to);
		}
		/*
		template<typename TCollection>
		static IEnumerator<T>* createEnumerator(TCollection& collection)
		{
			return createEnumerator<typename TCollection::iterator>(collection.begin(), collection.end());
		}

		template<typename TCollection>
		static IEnumerator<T>* createEnumerator(const TCollection& collection)
		{
			return createEnumerator<typename TCollection::const_iterator>(collection.begin(), collection.end());
		}
		*/
	public:
		Enumerator(Enumerator&& other)
			: _enumerator(other._enumerator)
		{}

		Enumerator(const Enumerator& other)
			: _enumerator(other._enumerator)
		{}

		template<typename TIterator>
		Enumerator(const TIterator& from, const TIterator& to)
			: _enumerator(createEnumerator(from, to))
		{}

		template<typename TCollection, typename = typename TCollection::iterator>
		Enumerator(TCollection& collection)
			: _enumerator(createEnumerator(collection.begin(), collection.end()))
		{}

		template<typename TCollection, typename = typename TCollection::const_iterator>
		Enumerator(const TCollection& collection)
			: _enumerator(createEnumerator(collection.begin(), collection.end()))
		{}

		Enumerator& operator=(const Enumerator& other)
		{
			if (this != &other)
			{
				this->_enumerator = other._enumerator;
			}
			return *this;
		}

		~Enumerator()
		{
			if (this->_enumerator.getReferenceCount() <= 1)
				delete this->_enumerator;
		}

		operator bool() const
		{
			return this->_enumerator->hasValue();
		}

		Enumerator& operator++()
		{
			this->_enumerator->moveNext();
			return *this;
		}

		T& operator*()
		{
			return this->_enumerator->value();
		}

		const T& operator*() const
		{
			return this->_enumerator->value();
		}

		T* operator->()
		{
			return &this->_enumerator->value();
		}

		const T* operator->() const
		{
			return &this->_enumerator->value();
		}

		T& value()
		{
			return this->_enumerator->value();
		}
		
		Enumerator<T>& filter(Action<bool, T&> predicate)
		{
			this->_enumerator = new FilterEnumerator<T>(this->_enumerator, predicate);
			return *this;
		}
	};
}
