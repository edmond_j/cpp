/*
Copyright Julien Edmond (05/06/2014)

edmondju@gmail.com

This library is a C++ library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#pragma once

#include <map>
#include <mutex>
#include "SmartPointerException.hpp"
#include "ScopedLock.hpp"

namespace My
{
	template<typename T>
	class SmartPointer
	{
	private:
		typedef std::map<T*, unsigned int> PointerStore;
		typedef typename PointerStore::iterator StoreIterator;

		static std::mutex _storeLock;
		static PointerStore _pointerStore;

		static void addReference(T* ptr)
		{
			ScopedLock lock(SmartPointer::_storeLock);
			StoreIterator it(SmartPointer::_pointerStore.find(ptr));

			if (it != SmartPointer::_pointerStore.end())
				++it->second;
			else
				SmartPointer::_pointerStore[ptr] = 1;
		}

		static void removeReference(T* ptr)
		{
			ScopedLock lock(SmartPointer::_storeLock);
			StoreIterator it(SmartPointer::_pointerStore.find(ptr));

			if (it != SmartPointer::_pointerStore.end())
				--it->second;
			else
				throw SmartPointerException(ptr, "Adress was not tracked.");
		}

		static unsigned int getReferenceCount(T* ptr)
		{
			ScopedLock lock(SmartPointer::_storeLock);
			StoreIterator it(SmartPointer::_pointerStore.find(ptr));

			if (it == SmartPointer::_pointerStore.end())
				throw SmartPointerException(ptr, "Adress was not tracked.");
			return it->second;
		}

		T* _ptr;

	public:
		SmartPointer()
			: _ptr(nullptr)
		{
			SmartPointer::addReference(this->_ptr);
		}

		SmartPointer(T* ptr)
			: _ptr(ptr)
		{
			SmartPointer::addReference(this->_ptr);
		}

		SmartPointer(const SmartPointer& other)
			: _ptr(other._ptr)
		{
			SmartPointer::addReference(this->_ptr);
		}

		SmartPointer& operator=(const SmartPointer& other)
		{
			if (*this != other)
			{
				SmartPointer::removeReference(this->_ptr);
				this->_ptr = other._ptr;
				SmartPointer::addReference(this->_ptr);
			}
			return *this;
		}

		~SmartPointer()
		{
			SmartPointer::removeReference(this->_ptr);
		}

		SmartPointer& operator=(T* ptr)
		{
			if (this->_ptr != ptr)
			{
				this->_ptr = ptr;
				SmartPointer::addReference(this->_ptr);
			}
			return *this;
		}

		bool operator==(const SmartPointer& other) const
		{
			return this->_ptr == other._ptr;
		}

		bool operator!=(const SmartPointer& other) const
		{
			return !this->operator==(other);
		}

		T& operator*()
		{
			return *this->_ptr;
		}

		const T& operator*() const
		{
			return *this->_ptr;
		}

		T* operator->()
		{
			return this->_ptr;
		}

		const T* operator->() const
		{
			return this->_ptr;
		}

		operator T*() const
		{
			return this->_ptr;
		}

		T* getPtr() const
		{
			return this->_ptr;
		}

		unsigned int getReferenceCount() const
		{
			return SmartPointer::getReferenceCount(this->_ptr);
		}
	};

	template<typename T>
	std::mutex SmartPointer<T>::_storeLock;

	template<typename T>
	typename SmartPointer<T>::PointerStore SmartPointer<T>::_pointerStore;
}
