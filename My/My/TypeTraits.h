#include <type_traits>

namespace My
{
	template<bool cond, typename Tif, typename Telse>
	struct IfElse
	{
		typedef Telse value;
	};

	template<typename Tif, typename Telse>
	struct IfElse<true, Tif, Telse>
	{
		typedef Tif value;
	};

	template<typename T>
	struct RemoveAllPR
	{
		typedef T value;
	};

	template<typename T>
	struct RemoveAllPR<T*>
	{
		typedef typename RemoveAllPR<T>::value value;
	};

	template<typename T>
	struct RemoveAllPR<T&>
	{
		typedef typename RemoveAllPR<T>::value value;
	};

	template<typename T>
	struct RemoveAllPR<T&&>
	{
		typedef typename RemoveAllPR<T>::value value;
	};
}